/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * A business network for trading and delivering multi-industry goods
 * The cargo is temperature controlled and contracts
 * can be negociated based on the temperature
 * readings received for the cargo
 */

namespace org.supplychain
////////////////////////////////////////////////////////////////////////////////////
//ASSET DEFINITIONS
/**
 * The type of product being delivered
 */
enum ProductType{
  	o BAGS
    o BALES
    o BANANAS
    o BARRELS
    o BOXES
    o CEMENT_BAGS
    o CONTAINERS
    o CRATES
    o DRUMS
    o EQUIPMENT
    o KEGS
    o MEDICINE
    o ORGAN
    o PARCELS
    o SACKS
    o TRAILERS
}

enum ProductTypeState {
  o FOR_SALE
  o RESERVE_NOT_MET
  o SOLD
}

/**
 * The status of a transit
 */
enum TransitStatus {
  o CREATED
  o IN_STORAGE
  o IN_TRANSIT
  o ARRIVED
}

/**
 * A transit being tracked as an asset on the ledger
 */
asset Transit identified by transitId {
  o String transitId
  o ProductType ProductType
  o ProductTypeState productState
  o Offer[] offers optional
  o TransitStatus status
  o Long unitCount
  o TemperatureReading[] temperatureReadings optional
  --> Contract contract

}

/**
 * Defines a contract between participants to transit 
 * , paying a set unit price. The unit price is multiplied by
 * a penality factor proportional to the deviation from the min and max
 * negociated temperatures for the transit.
 */
asset Contract identified by contractId {
  o String contractId
 //--> Pharmaceutical Personel
 //--> Banking Personel
 //--> Construction Personel
  o DateTime dispatchDateTime
  o DateTime arrivalDateTime
  o Double unitPrice
  o Double minTemperature optional
  o Double maxTemperature optional
  o Double minPenaltyFactor  
  o Double maxPenaltyFactor 
}

asset Product identified by productName{
  o String productName
  --> Product ProductType
  --> Product ProductTypeState
  o String productDimension
  o String productDescription
  o Double tradePrice 
   
}

////////////////////////////////////////////////////////////////////////////////////
//PARTICIPANT DEFINITIONS

/**
 * The type of perishable product being delivered
 */
enum Personel {
  o BANK
  o CLIENT
  o CONSUMER
  o CUSTOMER
  o CONTRACTOR
  o DISTRIBUTOR
  o FARMER
  o GROWER
  o HOSPITAL
  o MANUFACTURER
  o MARKET
  o PATIENT
  o PHARMACY
  o PROCESSOR
  o PRODUCER
  o RETAILER
  o SUPERMARKET
  o TREASURY
  o WAREHOUSE
}
/**
 * A concept for a simple street address
 */
concept Address {
  o String country 
  o String city
  o String street
  o String building optional
  o String floor optional
}
/**
 * A concept for sub-participant
 */
concept subParticipant {
  o String employeeName
  o String employeeID
  o String employeeEmail
  o String companyID optional  
  o String companyPhone optional

}

/**
 * An abstract participant type in this business network
 */
abstract participant Industry identified by companyName {
  o String companyName
  o String companyID
  o String companyEmail
  o String companyPhone
  o Address address
  o Personel SubParticipantType
  o subParticipant Personel
  o Double accountBalance
}

/**
 * A Pharmaceutical is a type of participant in the network
 */
participant Pharmaceutical extends Industry {
}

/**
 * A Banking is a type of participant in the network
 */
participant Banking extends Industry {
}

/**
 * An Construction is a type of participant in the network
 */
participant Construction extends Industry {
}

 

////////////////////////////////////////////////////////////////////////////////////
//TRANSACTIONS


// transaction Trade {
//  --> Transit transitId
  // --> product
// }

transaction Offer {
 o Industry Owner
 o Personel Owner_SubParticipantType
 --> Product productName
 --> Product ProductType
 --> Product ProductTypeState
 o Double tradePrice
 o Long unitCount
 --> Transit TransitStatus

}

/**
 * An abstract transaction that is related to a Transit
 */
abstract transaction TransitTransaction {
  --> Transit transit
  --> Product productName
  --> Product ProductType
  --> Product ProductTypeState
  
}

/**
 * An temperature reading for a transit. E.g. received from a
 * device within a temperature controlled delivering container
 */
transaction TemperatureReading extends TransitTransaction {
  o Double centigrade
}

/**
 * A notification that a transit has been received by the
 * importer and that funds should be transferred from the receiver
 * to the dispatcher to pay for the transit.
 */
transaction TransitReceived extends TransitTransaction {
   --> Industry companyName
  o Address ReceivedFrom
  o Double centigrade
}

/**
 * A notification that a Transit has been received by the
 * receiver and that funds should be transferred from the receiver
 * to the dispatcher to pay for the transit.
 */
transaction TransitDispatch extends TransitTransaction{
  o Double tradePrice
  --> Industry companyName
  --> Industry companyID
  o Address dispatchAddress
  o Double centigrade
}


/**
 * JUST FOR INITIALIZING A DEMO
 */
// transaction SetupDemo {
// }
