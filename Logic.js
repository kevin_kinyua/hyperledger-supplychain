/* global getParticipantRegistry getAssetRegistry  */
//getFactory - define it when using
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//TRANSIT RECEIVED

/**
 *  A transit has been received by an receiver
 * @param {org.supplychain.TransitReceived} TransitReceived - the TransitReceived transaction
 * @transaction
*/

async function transitReceived(transitReceived) {  

    const contract = transitReceived.transit.contract;
    const transit = transitReceived.transit;
    let payOut = contract.unitPrice * transit.unitCount;

   console.log('Received at: ' + transitReceived.timestamp);
   console.log('Contract arrivalDateTime: ' + contract.arrivalDateTime);

   // set the status of the transit
   transit.TransitStatus = 'ARRIVED';

   // if the transit did not arrive on time the payout is zero
   if (transitReceived.timestamp > contract.arrivalDateTime) {
       payOut = 0;
       console.log('Late Transit.');
   } else {
       // find the lowest temperature reading
       if (transit.temperatureReadings) {
           // sort the temperatureReadings by centigrade
           transit.temperatureReadings.sort(function (a, b) {
               return (a.centigrade - b.centigrade);
           });
           const lowestReading = transit.temperatureReadings[0];
           const highestReading = transit.temperatureReadings[transit.temperatureReadings.length - 1];
           let penalty = 0;
           console.log('Lowest temp reading: ' + lowestReading.centigrade);
           console.log('Highest temp reading: ' + highestReading.centigrade);

           // does the lowest temperature violate the contract?
           if (lowestReading.centigrade < contract.minTemperature) {
               penalty += (contract.minTemperature - lowestReading.centigrade) * contract.minPenaltyFactor;
               console.log('Min temp penalty: ' + penalty);
           }

           // does the highest temperature violate the contract?
           if (highestReading.centigrade > contract.maxTemperature) {
               penalty += (highestReading.centigrade - contract.maxTemperature) * contract.maxPenaltyFactor;
               console.log('Max temp penalty: ' + penalty);
           }

           // apply any penalities
           payOut -= (penalty * transit.unitCount);

           if (payOut < 0) {
               payOut = 0;
           }
       }
   }

   console.log('Payout: ' + payOut);
//    contract.pharmaceutical.accountBalance += payOut;
//    contract.construction.accountBalance -= payOut;
//    contract.banking.accountBalance -= payOut;
//    contract.Personel.accountBalance -= payOut;

//    console.log('Pharmaceutical: ' + contract.pharmaceutical.$identifier + ' new balance: ' + contract.pharmaceutical.accountBalance);
//    console.log('Construction: ' + contract.construction.$identifier + ' new balance: ' + contract.construction.accountBalance);
//    console.log('Banking: ' + contract.banking.$identifier + ' new balance: ' + contract.banking.accountBalance);


   // update the pharmaceutical's balance
   const pharmaceuticalRegistry = await getParticipantRegistry('org.supplychain.Pharmaceutical');
   await pharmaceuticalRegistry.update(contract.pharmaceutical);

   // update the construction's balance
   const constructionRegistry = await getParticipantRegistry('org.supplychain.Construction');
   await constructionRegistry.update(contract.construction);
 
    // update the banking's balance
   const bankingRegistry = await getParticipantRegistry('org.supplychain.Banking');
   await bankingRegistry.update(contract.banking);

   // update the state of the transit
   const transitRegistry = await getAssetRegistry('org.supplychain.Transit');
   await transitRegistry.update(transit);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//TRANSIT DISPATCHED

/**
 *  A transit has been dispatched by an receiver
 * @param {org.supplychain.TransitDispatch} TransitDispatch - the TransitDispatch transaction
 * @transaction
  */

async function dispatch(transitDispatch) {  // eslint-disable-line no-unused-vars

    console.log("Shalalala", transitDispatch);

    const contract = transitDispatch.transit.contract;
    const transit = transitDispatch.transit;

    let payOut = contract.unitPrice * transit.unitCount;

   console.log('Dispatched at: ' + transitDispatch.timestamp);
   console.log('Contract dispatchDateTime: ' + contract.dispatchDateTime);

//    // set the status of the transit
   transit.status = 'IN_TRANSIT';

//    // if the transit did not arrive on time the payout is zero
   if (transitDispatch.timestamp < contract.arrivalDateTime) {
       payOut = 0;
       console.log('In Transit.');
   } else {
       payOut = 100;
   }

    // Emit an event for the dispatched asset.
    // let event = dispatch().newEvent('org.supplychain', 'transitDispatch');
    // event.Product = tx.Product;
    // event.oldValue = oldValue;
    // event.accountBalance = tx.accountBalance;
    // event.dispatchDateTime = tx.dispatchDateTime;
    // emit(event);

    transitReceived(transitDispatch);

//    console.log('Payout: ' + payOut);
//    contract.pharmaceutical.Personel.accountBalance += payOut;
//    contract.construction.Personel.accountBalance -= payOut;
//    contract.banking.Personel.accountBalance -= payOut;
   //contract.Personel.accountBalance -= payOut;

//    console.log('Pharmaceutical: ' + contract.pharmaceutical.Personel.$identifier + ' new balance: ' + contract.pharmaceutical.Personel.accountBalance);
//    console.log('Construction: ' + contract.construction.Personel.$identifier + ' new balance: ' + contract.construction.Personel.accountBalance);
//    console.log('Banking: ' + contract.banking.Personel.$identifier + ' new balance: ' + contract.banking.Personel.accountBalance);


//    // update the pharmaceutical's balance
//    const pharmaceuticalRegistry = await getParticipantRegistry('org.supplychain.Pharmaceutical.Personel');
//    await pharmaceuticalRegistry.update(contract.pharmaceutical);

//    // update the construction's balance
//    const constructionRegistry = await getParticipantRegistry('org.supplychain.Construction.Personel');
//    await constructionRegistry.update(contract.construction);
 
    // update the banking's balance
//    const bankingRegistry = await getParticipantRegistry('org.supplychain.Banking.Personel');
//    await bankingRegistry.update(contract.banking);

//    // update the state of the transit
//    const transitRegistry = await getAssetRegistry('org.supplychain.Transit');
//    await transitRegistry.update(transit);
// }

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// //TEMPERATURE READING

// /**
// * A temperature reading has been received for a shipment
// * @param {org.supplychain.TemperatureReading} temperatureReading - the TemperatureReading transaction
// * @transaction
// */

// async function temperatureReading(temperatureReading) {  // eslint-disable-line no-unused-vars

//    const transit = temperatureReading.transit;

//    console.log('Adding temperature ' + temperatureReading.centigrade + ' to transit ' + transit.$identifier);

//    if (transit.temperatureReadings) {
//        transit.temperatureReadings.push(temperatureReading);
//    } else {
//        transit.temperatureReadings = [temperatureReading];
//    }

//    // add the temp reading to the shipment
//    const transitRegistry = await getAssetRegistry('org.supplychain.Transit');
//    await transitRegistry.update(transit);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//SET UP DEMO - NOT TO BE USED

/**
* Initialize some test assets and participants useful for running a demo.
* @param {org.supplychain.SetupDemo} setupDemo - the SetupDemo transaction
* @transaction
*/

/*create conditional statements for the sub participant fields
// */
// async function setupDemo(setupDemo) {  // eslint-disable-line no-unused-vars

//    const factory = getFactory();
//    const NS = 'org.supplychain';

//    // create the pharmaceutical
//    const pharmaceutical = factory.newResource(NS, 'Pharmaceutical', 'pharmacy');
//    const pharmaceuticalAddress = factory.newConcept(NS, 'Address');
//    pharmaceuticalAddress.country = 'KENYA';
//    pharmaceutical.address = pharmaceuticalAddress;
//    pharmaceutical.accountBalance = 0;

//    // create the construction
//    const construction = factory.newResource(NS, 'Construction', 'contractor');
//    const constructionAddress = factory.newConcept(NS, 'Address');
//    constructionAddress.country = 'KENYA';
//    construction.address = constructionAddress;
//    construction.accountBalance = 0;

//    // create the banker
//    const banking = factory.newResource(NS, 'Banking', 'banker');
//    const bankingAddress = factory.newConcept(NS, 'Address');
//    bankingAddress.country = 'KENYA';
//    banking.address = bankingAddress;
//    banking.accountBalance = 0;

//    // create the contract
//    const contract = factory.newResource(NS, 'Contract', 'CON_001');
//    contract.pharmaceutical = factory.newRelationship(NS, 'Pharmaceutical', 'pharmacy');
//    contract.construction = factory.newRelationship(NS, 'Construction', 'constractor');
//    contract.banking = factory.newRelationship(NS, 'Banking', 'banker');
//    const tomorrow = setupDemo.timestamp;
//    tomorrow.setDate(tomorrow.getDate() + 1);
//    contract.arrivalDateTime = tomorrow; // the shipment has to arrive tomorrow
//    contract.unitPrice = 0.5; // pay 50 cents per unit
//    contract.minTemperature = 2; // min temperature for the cargo
//    contract.maxTemperature = 10; // max temperature for the cargo
//    contract.minPenaltyFactor = 0.2; // we reduce the price by 20 cents for every degree below the min temp
//    contract.maxPenaltyFactor = 0.1; // we reduce the price by 10 cents for every degree above the max temp

//    // create the Transit
//    const transit = factory.newResource(NS, 'Transit', 'TRANSIT_001');
//    transit.type = 'BANANAS';
//    transit.status = 'IN_TRANSIT';
//    transit.unitCount = 5000;
//    transit.contract = factory.newRelationship(NS, 'Contract', 'CON_001');

//    // add the Pharmaceutical players
//    const pharmaceuticalRegistry = await getParticipantRegistry(NS + '.Pharmaceutical');
//    await pharmaceuticalRegistry.addAll([pharmaceutical]);

//    // add the Construction players
//    const constructionRegistry = await getParticipantRegistry(NS + '.Construction');
//    await constructionRegistry.addAll([construction]);

//    // add the Banking players
//    const bankingRegistry = await getParticipantRegistry(NS + '.Banking');
//    await bankingRegistry.addAll([banking]);

//    // add the contracts
//    const contractRegistry = await getAssetRegistry(NS + '.Contract');
//    await contractRegistry.addAll([contract]);

//    // add the transits
//    const transitRegistry = await getAssetRegistry(NS + '.Transit');
//    await transitRegistry.addAll([transit]);
// }


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//TRADE REGISTRY INTEGRATION - TO BE ADDED AFTER THE SUCCESS OF DISPATCH AND RECEIVAL TRANSACTIONS
/* global getAssetRegistry getParticipantRegistry */

/**
* Close the bidding for a product and choose the
* highest bid that is over the asking price
* @param {org.supplychain.Trade} Trade - the Trade transaction
* @transaction
*/

// async function Trade(Trade) {  // eslint-disable-line no-unused-vars
//    const Personel = Trade.Personel;
//    if (Personel.industry.product.productState !== 'FOR_SALE') {
//        throw new Error('Participant is not trading');
//    }

//    // by default we mark the ProductType as RESERVE_NOT_MET
//    ProductType.ProductTypeState = 'RESERVE_NOT_MET';
//    let highestOffer = null;
//    let buyer = null;
//    let seller = null;
   
//    if (v.offers && Transit.offers.length > 0) {
//        // sort the trades by tradePrice
//        Transit.offers.sort(function(a, b) {
//            return (b.tradePrice - a.tradePrice);
//        });
//        highestOffer = Transit.offers[0];
//        if (highestOffer.tradePrice >= product.reservePrice) {
//            // mark the product as SOLD
//            Product.ProductTypeState = 'SOLD';
//            buyer = highestOffer.Personel.industry;
//            seller = Transit.ProductType.Personel.industry;
//            // update the balance of the seller
//            console.log('#### seller balance before: ' + seller.balance);
//            seller.balance += highestOffer.tradePrice;
//            console.log('#### seller balance after: ' + seller.balance);
//            // update the balance of the buyer
//            console.log('#### buyer balance before: ' + buyer.balance);
//            buyer.balance -= highestOffer.tradePrice;
//            console.log('#### buyer balance after: ' + buyer.balance);
//            // transfer the product to the buyer
//            product.personel.industry = buyer;
//            // clear the offers
//            Product.offers = null;
//        }
//    }

//    if (highestOffer) {
//        // save the asset
//        const assetRegistry = await getAssetRegistry('org.supplychain.ProductTypeState');
//        await assetRegistry.update(Transit.ProductType);
//    }

//    // save the product transit
//    const transitRegistry = await getAssetRegistry('org.supplychain.ProductTypeState');
//    await transitRegistry.update(transit);

//    if (ProductType.ProductTypeState === 'SOLD') {
//        // save the buyer
//        const userRegistry = await getParticipantRegistry('org.supplychain.Personel');
//        await userRegistry.updateAll([buyer, seller]);
//    }
// }

/**
* Make an Offer for a ProductTypeState
* @param {org.supplychain.Offer} offer - the offer
* @transaction
*/
// async function makeOffer(offer) {  // eslint-disable-line no-unused-vars
//   let personel = offer.personel;
//    if (Product.personel !== 'FOR_SALE') {
//       throw new Error('Product is not FOR SALE');
//    }
//    if (!personel.offers) {
//    personel.offers = [];
//    }
//    personel.offers.push(offer);

//    //save the personel trade
//    const personel = await getAssetRegistry('org.supplychain.personel');
//    await personel.update(ProductTypeState);
// }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//TRADE MODULE

// async function tradeTransaction(tx) {  // eslint-disable-line no-unused-vars

//    // Save the old value of the asset.
//    const oldValue = tx.Product.value;

//    // Update the asset with the new value.
//    tx.Product.value = tx.newValue;

//    // Get the asset registry for the product.
//    const assetRegistry = await getAssetRegistry('org.supplychain.SampleAsset');
//    // Update the asset in the asset registry.
//    await assetRegistry.update(tx.asset);

//    // Emit an event for the modified asset.
//    let event = getFactory().newEvent('org.supplychain', 'TradeEvent');
//    event.Product = tx.Product;
//    event.oldValue = oldValue;
//    event.newValue = tx.newValue;
//    emit(event);
// }
